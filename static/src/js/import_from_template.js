odoo.define("somari.ImportFromTemplate", function (require) {
    "use strict";

    const FormRenderer = require("web.FormRenderer");
    const { Component } = owl;
    const { xml } = owl.tags;
    const { ComponentWrapper } = require("web.OwlCompatibility");
    const view_dialogs = require('web.view_dialogs');

    class ImportFromTemplate extends Component {
        static template = xml`<button t-on-click="clicked" class="btn btn-primary btn-sm">Import Form Library</button>`

        clicked(event){
            console.log('BUTTON CLICKED');
            // var self = this;
            // new view_dialogs.SelectCreateDialog(this, {
            //     res_model: 'somari.template', // the model name
            //     // title: "Select template", // dialog title, optional
            //     // domain: [], // domain to limit the record that can be selected, optional
            //     // no_create: true, // an option to make sure that user can not create a new record, optional
            //     on_selected: function (records) {
            //         console.log(records)
            //     }
            // }).open();
        }
     };

     FormRenderer.include({
         async _render() {
             await this._super(...arguments);
             for(const element of this.el.querySelectorAll(".somari_import_from_template")) {
                 (new ComponentWrapper(this, ImportFromTemplate))
                     .mount(element)
             }
         }
     });

 });
