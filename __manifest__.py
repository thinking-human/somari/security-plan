{
    'name': 'Somari - Security Plan',
    'author': 'Guillaume Deflaux',
    'version': '0.1',
    'category': 'Tools',
    'summary': 'Manage the security of your organizataion',
    'description': "",
    'website': 'https://somari.io',
    'depends': [
        'base',
        'web',
        'hr',
        'mail',
        'project'
    ],
    'data': [
        'views/staff_views.xml',
        'views/site_views.xml',
        'views/rating_category_views.xml',
        'views/risk_line_views.xml',
        'views/measure_views.xml',
        'views/emergency_contact_line_views.xml',
        'views/sop_views.xml',
        'views/security_plan_views.xml',
        'views/risk_views.xml',
        'views/risk_category_views.xml',
        'views/template_tag_views.xml',
        'views/template_views.xml',
        'views/menus.xml',
        'security/ir.model.access.csv',
        'data/risk.xml',
        'data/ir_sequence.xml'
    ],
    'assets': {
         'web.assets_backend': [
             'somari_sec_plan/static/src/js/import_from_template.js'
         ]
     },
    'qweb': [
        'static/src/xml/import_from_template.xml'
    ],
    'installable': True,
    'application': True
}
