# Somari - Security Plan

This Odoo module is a POC (Proof Of Concept) aimed at showing what a Security Plan application could look like. This is targetted at humanitarian organizations that have people on the field.

## What problem is the application addressing

When humanitarian workers travel to the field they are often given a security package and security briefing before departure. The core element is the Security Plan. It's often hard for organizations to keep these documents up to date and to share timely updates with their staff. This situation is a risk in itself for the staff and the organizations.

## How is the application addressing this problem

The application provides a single source of truth for structured security content. I should eventually include validation workflows, automatic notifications and a mobile application onto which staff would download all security content for a given country / site of operations.

## Demo video of the POC

Click image below.

[![](https://gitlab.com/thinking-human/somari/assets/-/raw/main/somari-splash.png)](https://www.youtube.com/watch?v=l_3XG11Ln4w)
