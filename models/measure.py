from odoo import fields, models, api
import logging

_logger = logging.getLogger(__name__)

class SomaryMeasureRating(models.Model):
    _name = "somari.measure.rating"
    _description = "Measure Rating"
    _order = "sequence"
    _rec_name = "rating_category_id"

    content = fields.Html(string="Content")
    measure_id = fields.Many2one("somari.measure", string="Measure")
    rating_category_id = fields.Many2one("somari.rating.category", string="Rating")
    rating_category_color = fields.Integer(related="rating_category_id.color")
    sequence = fields.Integer()

class SomariMeasure(models.Model):
    _name = "somari.measure"
    _description = "Mitigation Measure"
    _inherit = ["mail.thread", "mail.activity.mixin"]
    _order = "sequence"

    name = fields.Char(string="Name", required=True)
    sequence = fields.Integer()
    security_plan_id = fields.Many2one("somari.security_plan", required=True)
    rating_ids = fields.One2many("somari.measure.rating", "measure_id", string="Ratings")
    applied_rating_id = fields.Many2one("somari.measure.rating", domain="[('measure_id', '=', id)]", string="Applied Rating")
    applied_rating_color = fields.Integer(related="applied_rating_id.rating_category_id.color")
    content = fields.Html(related="applied_rating_id.content")

    _sql_constraints = [('name_unique', 'unique(name)', 'Name must be unique.')]

    @api.model
    def create(self, vals):
        ratings =[]
        rating_categories = self.env['somari.rating.category'].search([], order="sequence ASC")

        for rc in rating_categories:
            rating = {
                'content': "To be completed ({})".format(rc.name),
                'rating_category_id' : rc.id,
                'sequence': rc.sequence
            }
            ratings.append((0, 0, rating))
        vals['rating_ids'] = ratings

        vals['sequence'] = self.env['somari.measure'].search_count([('security_plan_id', '=', self.env.context['active_id'])])

        res = super(SomariMeasure, self).create(vals)
        return res

    def write(self, vals):
        msg = ""

        if 'name' in vals.keys():
            label = self._fields['name'].string
            msg += '<li>{}: {} <span class="fa fa-long-arrow-right"></span> {}</li>'.format(label, self.name, vals['name'])

        # Track changes on applied rating
        if 'applied_rating_id' in vals.keys():
            label = self._fields['applied_rating_id'].string
            old_val = self.applied_rating_id.rating_category_id.name
            new_val = self.env['somari.measure.rating'].browse(vals['applied_rating_id']).rating_category_id.name
            msg += '<li>{}: {} <span class="fa fa-long-arrow-right"></span> {}</li>'.format(label, old_val, new_val)

        # Track changes on rating category
        if 'rating_ids' in vals.keys():
            for r in vals['rating_ids']:
                if r[2]:
                    msg += "<li>Content updated for rating: {}</li>".format(self.env['somari.measure.rating'].browse(r[1]).rating_category_id.name)

        res = super(SomariMeasure, self).write(vals)

        # Publish tracked changes
        if msg != "":
            msg = "<ul>{}</ul>".format(msg)
            self.message_post(body=msg)

        return res
