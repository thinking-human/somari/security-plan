from odoo import fields, models, api

class SomaryTemplate(models.Model):
    _name = "somari.template"
    _description = "Template"

    name = fields.Char("Title", required=True)
    content = fields.Html("Content", required=True)
    type = fields.Selection(string= "Type",
        selection = [
        ('security_strategy', 'Security Strategy'),
        ('context', 'Context Analysis'),
        ('sop', 'SOP')
        ], required=True)
    tag_ids = fields.Many2many("somari.template.tag", string="Tags")
