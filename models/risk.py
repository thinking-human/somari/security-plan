from odoo import fields, models, api

class SomaryRisk(models.Model):
    _name = "somari.risk"
    _description = "Risk"

    name = fields.Char(string="Name", required=True)
    description = fields.Text(string="Description", required=True)
    risk_category_id = fields.Many2one("somari.risk_category", string="Category", required=True)

    _sql_constraints = [('name_unique', 'unique(name)', 'Name must be unique.')]
