from odoo import fields, models, api

class SomarySop(models.Model):
    _name = "somari.sop"
    _description = "Standard Operating Procedure"

    name = fields.Char(string="Name", required=True)
    content = fields.Html(string="Content", required=True)
    security_plan_id = fields.Many2one("somari.security_plan", required=True)
    tag_ids = fields.Many2many("somari.template.tag", string="Tags")

    _sql_constraints = [('name_unique', 'unique(name)', 'Name must be unique.')]
