from odoo import fields, models, api

class SomariStaff(models.Model):
    _inherit = "hr.employee"

    site_country_id = fields.Many2one("res.country", string="Country", required=True, readonly=False)
    site_id = fields.Many2one("somari.site", string="Site", required=True)
    security_focal_point = fields.Boolean("Security Focal Point")
    staff_type = fields.Selection(string="Staff Type",
        selection=[
            ('national', 'National'),
            ('expatriate', 'Expatriate')
        ], required=True)

    @api.onchange("site_country_id")
    def _clear_site_id(self):
        for r in self:
            r.site_id = False
