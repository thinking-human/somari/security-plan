from odoo import fields, models, api

class SomaryRiskCategory(models.Model):
    _name = "somari.risk_category"
    _description = "Risk Category"

    name = fields.Char(string="Name", required=True)
    color = fields.Integer()

    _sql_constraints = [('name_unique', 'unique(name)', 'Name must be unique.')]
