from odoo import fields, models, api
import logging

_logger = logging.getLogger(__name__)

STATUS_COLOR = {
    'draft': 4,  # light blue
    'in_review': 2,  # orange
    'validated': 20,  # green / success
    'rejected': 23  # red / danger
}

class SomariSecurityPlan(models.Model):
    _name = "somari.security_plan"
    _description = "Security Plan"
    _inherit = ["mail.thread", "mail.activity.mixin"]
    _order = "create_date DESC"

    name = fields.Char(string="Name", required=True, readonly=True, default='New')
    site_id = fields.Many2one("somari.site", string="Site", required=True)
    state = fields.Selection(string="Status",
        selection=[
            ('draft', 'Draft'),
            ('in_review', 'In Review'),
            ('validated', 'Validated'),
            ('rejected', 'Rejected')
        ],
        default="draft")
    color = fields.Integer(compute='_compute_color')
    responsible_user_id = fields.Many2one("res.users", string="Responsible", default=lambda self: self.env.user)
    validation_user_id = fields.Many2one("res.users", string="Validated by", readonly=True)
    validation_date = fields.Datetime(string="Validated on", readonly=True)
    security_strategy = fields.Html("Security Strategy")
    context = fields.Html("Context Analysis")
    risk_line_ids = fields.One2many("somari.risk.line", "security_plan_id", string="Risks")
    risk_line_count = fields.Integer(compute="_compute_risk_line_count")
    risk_line_present_count = fields.Integer(compute="_compute_risk_line_present_count")
    measure_count = fields.Integer(compute="_compute_measure_count")
    emergency_contact_ids = fields.One2many("somari.emergency.contact.line", "security_plan_id", string="Emergency Contacts")
    emergency_contact_count = fields.Integer(compute="_compute_emergency_contact_count")
    sop_ids = fields.One2many("somari.sop", "security_plan_id", string="SOPs")
    sop_count = fields.Integer(compute="_compute_sop_count")

    _sql_constraints = [('name_unique', 'unique(name)', 'Name must be unique.')]

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
           vals['name'] = self.env['ir.sequence'].next_by_code('somari.security_plan') or 'No Seq'
        vals['risk_line_ids'] = self.add_all_risks()
        res = super(SomariSecurityPlan, self).create(vals)
        return res

    def write(self, vals):
        msg = ""

        # Track changes on state
        if 'state' in vals.keys():
            label = self._fields['state'].string
            old_val = dict(self._fields['state'].selection).get(self.state)
            new_val = dict(self._fields['state'].selection).get(vals['state'])
            msg += '<li>{}: {} <span class="fa fa-long-arrow-right"></span> {}</li>'.format(label, old_val, new_val)

        # Track changes on responsible
        if 'responsible_user_id' in vals.keys():
            label = self._fields['responsible_user_id'].string
            old_val = self.responsible_user_id.name
            new_val = self.env['res.users'].browse(vals['responsible_user_id']).name
            msg += '<li>{}: {} <span class="fa fa-long-arrow-right"></span> {}</li>'.format(label, old_val, new_val)

        # Track changes on HTML fields
        for f in ['security_strategy', 'context']:
            if f in vals.keys() and eval("self.{}".format(f)) != vals[f]:
                msg += "<li>{}: Updated".format(self._fields[f].string)

        res = super(SomariSecurityPlan, self).write(vals)

        if msg != "":
            msg = "<ul>{}</ul>".format(msg)
            self.message_post(body=msg)

        return res

    def add_all_risks(self):
        risks = self.env['somari.risk'].search([], order="name")
        risk_lines = []
        for risk in risks:
            rl = {
                'risk_id': risk.id,
                'probability': 0,
                'impact': 0
            }
            risk_lines.append((0,0, rl))
        return risk_lines

    @api.depends('state')
    def _compute_color(self):
        for record in self:
            record.color = STATUS_COLOR[record.state]

    def action_open_sec_plan(self):
        return {
            'view_mode': 'form',
            'res_model': 'somari.security_plan',
            'res_id': self.id,
            'type': 'ir.actions.act_window',
            'context': self._context
        }

    def _compute_risk_line_count(self):
        RiskLine = self.env['somari.risk.line']
        for record in self:
            record.risk_line_count = RiskLine.search_count([('security_plan_id', '=', record.id)])

    def _compute_risk_line_present_count(self):
        RiskLine = self.env['somari.risk.line']
        for record in self:
            record.risk_line_present_count = RiskLine.search_count([('security_plan_id', '=', record.id), ('rating', '>', 0)])

    def _compute_measure_count(self):
        Measure = self.env['somari.measure']
        for record in self:
            record.measure_count = Measure.search_count([('security_plan_id', '=', record.id)])

    def _compute_emergency_contact_count(self):
        EmergencyContact = self.env['somari.emergency.contact.line']
        for record in self:
            record.emergency_contact_count = EmergencyContact.search_count([('security_plan_id', '=', record.id)])

    def _compute_sop_count(self):
        Sop = self.env['somari.sop']
        for record in self:
            record.sop_count = Sop.search_count([('security_plan_id', '=', record.id)])
