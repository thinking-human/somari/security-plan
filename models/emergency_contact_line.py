from odoo import fields, models, api

class SomaryEmergencyStaffLine(models.Model):
    _name = "somari.emergency.contact.line"
    _description = "Emergency Contact"
    _order = "sequence"

    security_plan_id = fields.Many2one("somari.security_plan", required=True)
    contact_id = fields.Many2one("hr.employee", string="Name", required=True, domain=[('site_id', '!=', False)])
    comment = fields.Char("Comments")
    sequence = fields.Integer("Sequence")
    job_title = fields.Char(related="contact_id.job_title")
    email = fields.Char(related="contact_id.work_email")
    mobile_phone = fields.Char(related="contact_id.mobile_phone")
    site = fields.Char(compute="_compute_base")

    def _compute_base(self):
        for r in self:
            r.site = "{} / {}".format(r.contact_id.site_country_id.name, r.contact_id.site_id.name)
