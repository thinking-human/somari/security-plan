from odoo import fields, models, api

class SomaryTemplateTag(models.Model):
    _name = "somari.template.tag"
    _description = "Template Tag"
    _order = "name"

    name = fields.Char(string="Name", required=True)
    color = fields.Integer()

    _sql_constraints = [('name', 'unique(name)', 'Name must be unique!')]
