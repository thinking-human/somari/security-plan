from odoo import fields, models, api

class SomarySite(models.Model):
    _name = "somari.site"
    _description = "Site"
    _order = "name"

    name = fields.Char(string="Name", required=True)
    staff_ids = fields.One2many("hr.employee", "site_id", string="Staff")
    staff_nationals = fields.Integer(string="National Staff", compute="_compute_staff_count")
    staff_expats = fields.Integer(string="Expatriate Staff", compute="_compute_staff_count")
    staff_count = fields.Integer(compute="_compute_staff_count")
    vehicules = fields.Integer(string="Vehicules")
    security_plan_ids = fields.One2many("somari.security_plan", "site_id", string="Security Plans")
    # Address fields
    street = fields.Char()
    street2 = fields.Char()
    zip = fields.Char(change_default=True)
    city = fields.Char()
    state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict', domain="[('country_id', '=?', country_id)]")
    country_id = fields.Many2one("res.country", string="Country", required=True, ondelete='restrict')
    country_code = fields.Char(related='country_id.code', string="Country Code")

    _sql_constraints = [('name_unique', 'unique(name)', 'Name must be unique.')]

    def _compute_staff_count(self):
        Staff = self.env['hr.employee']
        for r in self:
            r.staff_count = Staff.search_count([('site_id', '=', self.id)])
            r.staff_expats = Staff.search_count([('site_id', '=', self.id), ('staff_type', '=', 'expatriate')])
            r.staff_nationals = Staff.search_count([('site_id', '=', self.id), ('staff_type', '=', 'national')])
