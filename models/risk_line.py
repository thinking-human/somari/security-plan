from odoo import fields, models, api

class SomaryRiskLine(models.Model):
    _name = "somari.risk.line"
    _description = "Risk Line"

    security_plan_id = fields.Many2one("somari.security_plan", required=True)
    risk_category_id = fields.Many2one("somari.risk_category", string="Risk Category", related="risk_id.risk_category_id", store=True)
    risk_id = fields.Many2one("somari.risk", string="Risk", readonly=True)
    probability = fields.Integer(required=True, group_operator="")
    impact = fields.Integer(required=True, group_operator="")
    rating = fields.Integer(compute="_compute_rating", store=True, group_operator="")
    rating_category_id = fields.Many2one("somari.rating.category", string="Rating Category", compute="_compute_rating", store=True)
    rating_color = fields.Integer(related="rating_category_id.color")

    _sql_constraints = [
            ('check_probability', 'CHECK(probability >= 0 AND probability <= 5)',
             'The probability should be between 0 and 5.'),
             ('check_impact', 'CHECK(impact >= 0 AND impact <= 5)',
              'The impact should be between 0 and 5.')
        ]

    @api.depends("probability", "impact")
    def _compute_rating(self):
        for r in self:
            r.rating = r.impact * r.probability

            RatingCategory = self.env['somari.rating.category']
            coordinates = "({},{})".format(r.impact, r.probability)
            cat = RatingCategory.search([('coordinates', 'like', coordinates)], limit=1)
            for c in cat:
                r.rating_category_id = c.id
