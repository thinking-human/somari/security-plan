from odoo import fields, models, api

class SomariRatingCategory(models.Model):
    _name = "somari.rating.category"
    _description = "Risk Rating Category"

    name = fields.Char(required=True)
    color = fields.Integer(required=True)
    sequence = fields.Integer(required=True)
    coordinates = fields.Char(required=True) # (impact,probability)
